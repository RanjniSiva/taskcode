 var STATIC_APP_URL = 'https://phplaravel-147120-711451.cloudwaysapps.com'; 
// var STATIC_APP_URL = 'https://app.vouchermatic.com'; 		// Live url (APP)
//var STATIC_APP_URL = 'https://staging.vouchermatic.com'; 		// Live url (STAGING)



// Get the modal
var modal = document.getElementById('myModal');
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closed")[0];
function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};
function openTab(url) {
    // Create link in memory
    var a = window.document.createElement("a");
    a.target = '_blank';
    a.href = url;
 
    // Dispatch fake click
    var e = window.document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
};

function isFacebookApp() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
}


// When the user clicks the button, open the modal 
function openPopup() {
	$('#BookingtoolBookeDetail').modal('hide');
	var bookeddate = document.getElementById("booked_date").innerText;
	var staffname = document.getElementById("staffname").innerText;  
	var duration = document.getElementById("duration").innerText;  
	var starttime = document.getElementById("starttime").innerText;  
	var resource_name = document.getElementById("resource_name").innerText;  
	var staffid = document.getElementById("staffid").value;  
	var locationid = document.getElementById("locationid").value;  
	var end_time = document.getElementById("end_time").value;  
	var booking_id = document.getElementById("booking_id").value;  
	var resource_id = document.getElementById("resource_id").value; 
	var booking_member = document.getElementById("booking_member").value; 
	var capacity = document.getElementById("capacity").value; 
	console.log(resource_name);
	console.log(resource_id);
	console.log(booking_member);
	var res = duration.split("@");
	var dur = res[0];
	var locations = res[1];
	
	var Time = starttime.split("to");
	var start = Time[0];
	var end = Time[1];
	
	var ubf = browseinfo.browser.family;
	var udf = browseinfo.device.family;
	// var url = STATIC_APP_URL+'/BookingToolPage/'+bookeddate+'/'+staffid+'/'+dur+'/'+locationid+'/'+start+'/'+locations+'/'+staffname+'/'+resource_name+'/'+resource_id;
	var url = STATIC_APP_URL+'/BookingToolPage/'+bookeddate+'/'+staffid+'/'+dur+'/'+locationid+'/'+start+'/'+end_time+'/'+locations+'/'+staffname+'/'+booking_id+'/'+resource_name+'/'+resource_id+'/'+booking_member+'/'+capacity;
	if(isMobileDevice() == true){
		jQuery(document).ready(function($){
			if(isFacebookApp() == true){
				window.top.location.href = url;			
			}	
			else{
				if(udf.toLowerCase().indexOf('ipad') > -1 ||udf.toLowerCase().indexOf('iphone') > -1){
					openTab(url);
				}
				else{
					$('#iframeModal').append('<iframe id="iframeContent" src="'+STATIC_APP_URL+'/BookingToolPage/'+bookeddate+'/'+staffid+'/'+dur+'/'+locationid+'/'+start+'/'+end_time+'/'+locations+'/'+staffname+'/'+booking_id+'/'+resource_name+'/'+resource_id+'/'+booking_member+'/'+capacity+'"></iframe>');
					modal.style.display = "block";
				}
			}
		});
	}
	else {
		jQuery(document).ready(function($){
			var url = STATIC_APP_URL+'/BookingToolPage/'+bookeddate+'/'+staffid+'/'+dur+'/'+locationid+'/'+start+'/'+end_time+'/'+locations+'/'+staffname+'/'+booking_id+'/'+resource_name+'/'+resource_id+'/'+booking_member+'/'+capacity;
			if(browseinfo.browser.family == 'Safari'){
				openTab(storeVoucherUrl);
			}
			else {
				$('#iframeModal').append('<iframe id="iframeContent" src="'+STATIC_APP_URL+'/BookingToolPage/'+bookeddate+'/'+staffid+'/'+dur+'/'+locationid+'/'+start+'/'+end_time+'/'+locations+'/'+staffname+'/'+booking_id+'/'+resource_name+'/'+resource_id+'/'+booking_member+'/'+capacity+'"></iframe>');
				modal.style.display = "block";
			}
		});
	}
}


jQuery(document).ready(function($){
	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$('#iframeModal').html('');
		modal.style.display = "none";
		location.reload();
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			$('#iframeModal').html('');
			modal.style.display = "none";
			location.reload();
		}
	}
});
