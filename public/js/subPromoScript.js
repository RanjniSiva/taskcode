var STATIC_APP_URL = 'https://phplaravel-147120-711451.cloudwaysapps.com'; 
//var STATIC_APP_URL = 'https://staging.vouchermatic.com'; 		// Live url (STAGING)
 // var STATIC_APP_URL = 'https://app.vouchermatic.com'; 		// Live url (APP)
// var STATIC_APP_URL = 'http://localhost/onlinebooking/public'; 

 //alert(browseinfo.device.family);
 
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];


 
function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};
function openTab(url) {
    // Create link in memory
    var a = window.document.createElement("a");
    a.target = '_blank';
    a.href = url;
 
    // Dispatch fake click
    var e = window.document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
};

function isFacebookApp() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
}

 // When the user clicks the button, open the modal 
function openPromoPopup(randomKey,storeId,voucherId) {
	
	var ubf = browseinfo.browser.family;
	var udf = browseinfo.device.family;
	
	var url = STATIC_APP_URL+'/promotional_vouchers/'+randomKey+'/'+storeId+'/'+voucherId;
	if(isMobileDevice() == true){
		jQuery(document).ready(function($){
		
			if(isFacebookApp() == true){
				window.top.location.href = url;			
			}	
			else{
				if(udf.toLowerCase().indexOf('ipad') > -1 ||udf.toLowerCase().indexOf('iphone') > -1){
					openTab(url);
				}
				else{
					$('#iframeModal').append('<iframe id="iframeContent" src="'+STATIC_APP_URL+'/promotional_vouchers/'+randomKey+'/'+storeId+'/'+voucherId+'"></iframe>');
					modal.style.display = "block";
				}
			}
		});
	}
	else {
		jQuery(document).ready(function($){
			
			if(browseinfo.browser.family == 'IE'){
				/* alert('Please enable Third-Party-Cookies for your browser or use different browser.');
				return false; */
				$('#iframeModal').append('<iframe id="iframeContent" src="'+STATIC_APP_URL+'/promotional_vouchers/'+randomKey+'/'+storeId+'/'+voucherId+'"></iframe>');
				modal.style.display = "block";
			}
			else if(browseinfo.browser.family == 'Safari'){
				openTab(url);
			}
			else {
				$('#iframeModal').append('<iframe id="iframeContent" src="'+STATIC_APP_URL+'/promotional_vouchers/'+randomKey+'/'+storeId+'/'+voucherId+'"></iframe>');
				modal.style.display = "block";
			}
		});
	}
}

jQuery(document).ready(function($){
	
	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$('#iframeModal').html('');
		modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			$('#iframeModal').html('');
			modal.style.display = "none";
		}
	}
	
});
