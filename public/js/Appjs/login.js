var app = angular.module('organization', []);

app.controller('HomeController', function ($scope, $http,$location) { 



	$(document).bind('keypress', function(e) {
		var login={};
		if (e.which == 13) {
			login["username"]=angular.element("#sys_email").val();//$scope.accountLogin.username;
			login["password"]=angular.element("#sys_pass").val();//$scope.accountLogin.password
			$scope.loginfunction(login);
			return false;   
		}
	});




	$scope.div_login=false;

	$scope.checklogin=function(login)
	{
		$scope.loginfunction(login);
	};

	
	
	$scope.loginfunction = function (login)
	{
		if( !login.username ||  !login.password )
			{
				if(login.username)
				{
					$scope.loginmsg="Password Field Shouldnot be Empty";
					return false;
				}
				else
				{
					$scope.loginmsg="Username Should not be Empty";
					return false;
				}				
			}
		else{
			$http({
				method: 'POST',
				url: APP_URL+'/authentication',
				async: false,
				params: { loginData: login },
				//headers:{Authorization: "Bearer MjE4ODI2MjQ4"}
			}).success(function (data) {
				
				$("#msg_div").fadeOut(100);
				
				$("#msg_div").fadeIn(200);
				
				
				if(data.l_status == 0)
				{
					$scope.loginmsg="E-mail has not been Registered with Us";
				}
				else if(data.l_status == 1)
				{
					$scope.loginmsg="Your E-mail Needs to be verified";
				}
				else if(data.l_status == 2)
				{
					$scope.loginmsg="Invalid Password";
				}
				
				else{
					window.location.href = APP_URL+'/manageusers';
				}
			});
		}	
	}
	

});

