var app = angular.module('organization', []);
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});	


app.controller('manageusers', function ($scope, $http, $filter, $timeout) {
	
	
	
	angular.element(document).ready(function () {
        $scope.FetchDataOnLoad();
    });
	$scope.FetchDataOnLoad = function() {
		$http({
			method: 'GET',
			url: APP_URL+'/GetuserDetails',
			async: false,
			params: {},
		}).success(function (data) {
			if(data.retdata == 1){
				$scope.userlist = data.userlist;
			}else{
				$scope.userlist = [];
			}
		});
	}
	
	$scope.edituser = function(userlist) {
		$scope.userfirstname = userlist.firstname;
		$scope.userlastname = userlist.lastname;
		$scope.userdtorename = userlist.store_name;
		$scope.useremail = userlist.email;
		
	}
	
	
	// update user details
	$scope.updateuser = function(userfirstname,userlastname,userdtorename,useremail) {
		if(userfirstname == "" || userfirstname == undefined || userlastname == "" || userlastname == undefined || userdtorename == "" || userdtorename == undefined || useremail == "" || useremail == undefined){
			$scope.updErrMsg = "Fill All Mandatory Details!";
		}
		else {
			$scope.updErrMsg = "";
			$scope.userfirstname = userfirstname;
			$scope.userlastname = userlastname;
			$scope.userdtorename = userdtorename;
			$scope.useremail = useremail;
			
			$http({
				method: 'GET',
				url: APP_URL+'/updateuser',
				async: false,
				params: { userfirstname: $scope.userfirstname,userlastname: $scope.userlastname,userdtorename: $scope.userdtorename,useremail: $scope.useremail},
			}).success(function (data) {
				if(data.retdata == 1){
					$scope.timesucess = "User Updated Sucessfully.";
					$('#Edituser').modal('hide');
					$scope.FetchDataOnLoad();
				}
				
			});
		}
	}
	
	// DELETE THIS PRODUCT
	$scope.deleteuser = function (storeid){
		$scope.hidstoreid = storeid;
	}
	
	
	$scope.confirmDelete = function(storeid) {
		$http({
			method: 'GET',
			url: APP_URL+'/DeleteUser',
			async: false,
			params: { storeid: storeid },
		}).success(function (data) {
				if(data == 1){
					$scope.StatusMsg = 'User Removed Successfully!';
					$scope.StatusErrMsg = '';
					$scope.FetchDataOnLoad();
				}
				else {
					$scope.StatusMsg = '';
					$scope.StatusErrMsg = 'Not Deleted!';
				}
			
		});
	}
	
	 
	
	
});