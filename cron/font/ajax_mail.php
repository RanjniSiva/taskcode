<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
<?php 
$base = dirname(dirname(__FILE__)); // now $base contains "app"
set_include_path($base.'/public/mailgun-php/vendor/');
require 'autoload.php';
use Mailgun\Mailgun;
include 'db_connection.php'; 

$conn = OpenCon();
//print_r($_POST);
 if(isset($conn))
{
	
	$sqlOrder= "SELECT * FROM `m_order` where m_order.payment_recieved ='1' and pdf_generated='1' and email_sent ='0' limit 1";
	$dataOrder = mysqli_query($conn, $sqlOrder);
	
	$rowOrder=mysqli_fetch_assoc($dataOrder);
	if( mysqli_num_rows($dataOrder) >0)
	{
		$store_id = $rowOrder['store_id'];
		$orderID = $rowOrder['order_id'];
		
		$sqlSelectedCurrency = "select currencies.symbol from m_users 
									join currencies on m_users.currency=currencies.id
								where m_users.user_id = $store_id ";
		$dataSelectedCurrency = mysqli_query($conn, $sqlSelectedCurrency);
		if( mysqli_num_rows($dataSelectedCurrency) >0)
		{
			while ($getSelectedCurrency=mysqli_fetch_assoc($dataSelectedCurrency))
			{
				$selectedCurrency = $getSelectedCurrency['symbol'];
			}
		}
		else{
				$selectedCurrency=  "$";
		}
			
	
		$sqlUsers = "SELECT *  FROM m_users where m_users.user_id = $store_id ";
		$dataUsers = mysqli_query($conn, $sqlUsers);
		if( mysqli_num_rows($dataUsers) >0)
		{
			while ($rowUsers=mysqli_fetch_assoc($dataUsers))
			{
				$store_name = $rowUsers['store_name'];
			}
		}
			$base_path = dirname(dirname(__FILE__)).'/public/img/ordered_vouchers/custom_pdf/';
			$sqlOrdDetails = "SELECT 
								order_details.*, m_order.*, m_voucher_details.v_description, m_voucher_details.v_name, m_voucher_details.v_expire_date, m_voucher_details.v_terms, m_voucher_details.v_type, m_voucher_details.v_type, m_voucher_details.v_product_id 
							FROM order_details 
							INNER JOIN m_voucher_details ON order_details.voucher_id = m_voucher_details.voucher_id 
							INNER JOIN m_order on order_details.order_id = m_order.order_id 
							WHERE order_details.order_id = $orderID  							
							ORDER BY order_details.created_date ";// and order_details.delivery_email = '1'
			$dataOrdDetails = mysqli_query($conn, $sqlOrdDetails );
			if(!empty($dataOrdDetails))
			{
				$customerHtml = '';
				$response['status'] = '';
				if(!empty($rowOrder)){
					$customerEmail = $rowOrder['customer_email']; 
					$sqlCustomers = "SELECT *  FROM m_store_customer where customer_email='".$customerEmail."' and store_id = $store_id ";
					$dataCustomers = mysqli_query($conn, $sqlCustomers);
					if( mysqli_num_rows($dataCustomers) >0)
					{
						while ($rowCustomers=mysqli_fetch_assoc($dataCustomers))
						{
							$customerName = $rowCustomers['firstname']." ".$rowCustomers['lastname'];
						}
					}
					$customerHtml .= '<div style="background-color:#f5f5f5; font-family:calibri;">';
						$customerHtml .= '<div style="padding:10px 0;margin:0 auto;overflow-x:auto;max-width:800px;">';
							$customerHtml .= '<div style="background-color:#93c890; height:50px;">';
								$customerHtml .= '<table style="width:100%;margin:0 auto;height:50px; background-color:#93c890;">';
									$customerHtml .= '<tbody>';
										$customerHtml .= '<tr>';
											$customerHtml .= '<td style="text-align:center;">';
												$customerHtml .= '<p><h2 style="color: #FFF; text-transform:uppercase; font-family:calibri;">Order Placed</h2></p>';
											$customerHtml .= '</td>';
										$customerHtml .= '</tr>';
									$customerHtml .= '</tbody>';
								$customerHtml .= '</table>';
							$customerHtml .= '</div>';
							$customerHtml .= '<div style="background-color:#ffffff; padding-bottom:20px;">';
								$customerHtml .= '<table style="width:95%;padding-left:15px;font-size:18px;">';
									$customerHtml .= '<tbody>';
										$customerHtml .= '<tr>';
											$customerHtml .= '<td style="text-align:center;">';
												$customerHtml .= '<p><h4 style="color: #4b4c4f; font-family:calibri;">Hi '.  $customerName .', thank you for purchasing the following vouchers from  <span style="text-transform: uppercase; color:#000;">'.$store_name.'</span>,</h4></p>';
											$customerHtml .= '</td>';
										$customerHtml .= '</tr>';
									$customerHtml .= '</tbody>';
								$customerHtml .= '</table><br/>';
								
								$customerHtml .= '<table style="width:97%;padding-left:15px; padding-top:20px; border-collapse: collapse; font-size:16px;" align="center">';
									$customerHtml .= '<tbody>';
										$customerHtml .= '<tr style="border-bottom:1px solid #000; border-top:1px solid #000;">';
											$customerHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">#</th>';
											$customerHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Voucher</th>';
											$customerHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Recipient Name</th>';
											$customerHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Recipient Email/Postal Address</th>';
											$customerHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Quantity</th>';
											$customerHtml .= '<th style="text-align:right; padding-top:10px; padding-bottom:10px;">Price</th>';
										$customerHtml .= '</tr>';
										
										$totalItemPrice = 0;$index = 0;
										while ($rowOrderDetails = mysqli_fetch_array($dataOrdDetails))
										{
											$itemprice = $rowOrderDetails['voucher_subtotal'];
											$qtyItmPrice = ($rowOrderDetails['voucher_subtotal'] * $rowOrderDetails['quantity']);
											$index = $index + 1;
											
											$customerHtml .= '<tr style="font-size:14px; border-bottom:1px solid #DDD;">';
												$customerHtml .= '<td style="text-align:center; padding:5px 0px;">'.$index.'</td>';
												$customerHtml .= '<td style="text-align:center; text-transform:capitalize;">'.$rowOrderDetails['v_name'].'</td>';
												$customerHtml .= '<td style="text-align:center; text-transform:capitalize;">'.$rowOrderDetails['recipient_name'].'</td>';
												if($rowOrderDetails['delivery_post'] ==1)
												{	
													$recp_address1 = $rowOrderDetails['recp_address1'];
													$recp_address2 = $rowOrderDetails['recp_address2'];
													$recp_city = $rowOrderDetails['recp_city'];
													$recp_country =  $rowOrderDetails['recp_country'];
													$recp_zipcode = $rowOrderDetails['recp_zipcode'];
													
													$recp_address1 = ($recp_address1=='-') ? "": $recp_address1;
													$recp_address2 = ($recp_address2=='-') ? "": $recp_address2;
													$recp_city = ($recp_city=='-') ? "": $recp_city;
													$recp_country = ($recp_country=='-') ? "": $recp_country;
													$recp_zipcode = ($recp_zipcode=='-') ? "": $recp_zipcode;
													$fulladdress = implode(", ", array_filter([$recp_address1 , $recp_address2, $recp_city, $recp_country, $recp_zipcode]));
													
													$customerHtml .= '<td style="text-align:center;">'.$fulladdress.'</td>';
												}else{
													$customerHtml .= '<td style="text-align:center;">'.$rowOrderDetails['recipient_email'].'</td>';
												}
												$customerHtml .= '<td style="text-align:center;">'.$rowOrderDetails['quantity'].'</td>';
												$customerHtml .= '<td style="text-align:right;">'.$selectedCurrency.' '.number_format($itemprice, 2).'</td>';
											$customerHtml .= '</tr>';
											
											$totalItemPrice += $qtyItmPrice;
											
										}
										//$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:0px solid #DDD;"><td colspan = 6>&nbsp;</td</tr>';
										$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:0px solid #DDD;">';
											$customerHtml .= '<td colspan=5 style="text-align:right; padding:10px 0px;">Voucher Total : </td>';
											$customerHtml .= '<td style="text-align:right;">'.$selectedCurrency.' '.number_format($totalItemPrice, 2).'</td>';
										$customerHtml .= '</tr>';
										$totalAfterDiscount = $rowOrder['voucher_total_amount'];
										if($rowOrder['discount_amt'] > 0){
											$discountAmt = $rowOrder['discount_amt'];
											$totalAfterDiscount = $rowOrder['voucher_total_amount'] - $discountAmt;
											$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:0px solid #DDD;">';
													$customerHtml .= '<td colspan=5 style="text-align:right; padding:10px 0px;"> Discount Amount : </td>';
											$customerHtml .= '<td style="text-align:right;">(-) '.$selectedCurrency.' '.number_format($rowOrder['discount_amt'], 2).'</td>';
											$customerHtml .= '</tr>';
										}
										if($rowOrder['final_postage_charges'] >0)
										{
											$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:1px solid #DDD;">';
												$customerHtml .= '<td colspan=5 style="text-align:right; padding:10px 0px;">Postage Charges : </td>';
												$customerHtml .= '<td style="text-align:right;">'.$selectedCurrency.' '.number_format($rowOrder['final_postage_charges'], 2).'</td>';
											$customerHtml .= '</tr>';
										}
										$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:1px solid #DDD;">';
											$customerHtml .= '<td colspan=5 style="text-align:right; padding:10px 0px;">Payment Processing Fee : </td>';
											$customerHtml .= '<td style="text-align:right;">'.$selectedCurrency.' '.number_format($rowOrder['final_voucher_charges'], 2).'</td>';
										$customerHtml .= '</tr>';
										$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:1px solid #DDD;">';
											$customerHtml .= '<td colspan=5 style="text-align:right; padding:10px 0px;">Final Total : </td>';
											$customerHtml .= '<td style="text-align:right;">'.$selectedCurrency.' '.number_format($totalAfterDiscount, 2).'</td>';
										$customerHtml .= '</tr>';
										$customerHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:1px solid #DDD;">';
										$customerHtml .= '<td colspan=6 style="text-align:right; padding:10px 0px;"><h5><i>'.$rowOrder['discount_message'].' </i><h5></td>';
									
										$customerHtml .= '</tr>';
									$customerHtml .= '</tbody>';
								$customerHtml .= '</table>';
							$customerHtml .= '</div>';
							$customerHtml .= '<div style="background-color:#ffffff; padding-bottom:20px; text-align:center;">';
								$customerHtml .= '<table style="width:100%;">';
									$customerHtml .= '<tbody>';
										$customerHtml .= '<tr>';
											$customerHtml .= '<td style="width:100%;font-size:11px;text-align:center;color:#ccc;">';
											$customerHtml .= 'Powered by &nbsp;&nbsp;<a style="text-decoration:none!important;" href="http://www.vouchermatic.com"><span style="font-family:\'Prompt\', sans-serif; font-size : 16px; color:#3AB2BC; letter-spacing:1.5px;">Voucher</span><span style="font-family:\'Prompt\', sans-serif; font-size : 16px; color:#1A5358; letter-spacing:1.5px;">matic</span> </a>';
											$customerHtml .= '</td>';
										$customerHtml .= '</tr>';
									$customerHtml .= '</tbody>';
								$customerHtml .= '</table>';
							$customerHtml .= '</div>';
						$customerHtml .= '</div>';
					$customerHtml .= '</div>';
					//print $customerHtml;
					$mgClient = new Mailgun('key-0199990b49e054135482be3d479c5864');
					$domain = "https://api.mailgun.net/v3/mg.vouchermatic.com";

					# Make the call to the client.
					 $mail = $mgClient->sendMessage("$domain",
							array('from' => ''.$store_name.'<admin@vouchermatic.com>',
							'to'      => ''.$customerEmail.' <'.$customerEmail.'>',
							'subject' => 'Order Placed',
							'html'    => $customerHtml)); 
					
					if($mail) {
						$response['status'] = '1';
						// $response = 'Order Placed Successfully';
						//$mailResponse = $this->sendmailtorecipient($orderID, $store_name, $customerEmail);
						//return $mailResponse; 
						$sqlDistData = "SELECT * FROM order_details WHERE order_id= $orderID AND delivery_email='1' GROUP BY recipient_email";
						$dataDistDetails = 	mysqli_query($conn,$sqlDistData);
							if(mysqli_num_rows($dataDistDetails)>0){
								$recpEmail = '';
								while ($rowDistDetails = mysqli_fetch_assoc($dataDistDetails))
								{
									
									// $ordTransId = $rowDistDetails['id;
									$orderDetailID = $rowDistDetails['id'];
									$orderID = $rowDistDetails['order_id'];
									$recpEmail = $rowDistDetails['recipient_email'];
									//print "<br>".$recpEmail."<br>";
									$recipientName = $rowDistDetails['recipient_name'];
									// $dataForEmail = DB::table('order_details')->where('cart_session_id',$orderdetailID)->where('recipient_email',$recpEmail)->get();
									$recipientHtml = '';
										$recipientHtml .= '<div style="background-color:#f5f5f5; font-family:calibri;">';
										$recipientHtml .= '<div style="padding:10px 0;margin:0 auto;overflow-x:auto;max-width:800px;">';
											$recipientHtml .= '<div style="background-color:#93c890; height:50px;">';
												$recipientHtml .= '<table style="width:100%;margin:0 auto;height:50px; background-color:#93c890;">';
													$recipientHtml .= '<tbody>';
														$recipientHtml .= '<tr>';
															$recipientHtml .= '<td style="text-align:center;">';
																$recipientHtml .= '<p><h2 style="color: #FFF; text-transform:uppercase; font-family:calibri;">Gift Voucher</h2></p>';
															$recipientHtml .= '</td>';
														$recipientHtml .= '</tr>';
													$recipientHtml .= '</tbody>';
												$recipientHtml .= '</table>';
											$recipientHtml .= '</div>';
											$recipientHtml .= '<div style="background-color:#ffffff; padding-bottom:20px;">';
												$recipientHtml .= '<table style="width:95%;padding-left:15px;font-size:18px;">';
													$recipientHtml .= '<tbody>';
														$recipientHtml .= '<tr>';
															$recipientHtml .= '<td style="text-align:center;">';
																$recipientHtml .= '<p><h4 style="color: black; font-family:calibri;">Hello '.$recipientName.' , '.$customerName.' has sent you a gift voucher from  <span style="text-transform: uppercase; color:#000;">'.$store_name.'</span> </h4></p>';
															$recipientHtml .= '</td>';
														$recipientHtml .= '</tr>';
													$recipientHtml .= '</tbody>';
												$recipientHtml .= '</table>';
												$recipientHtml .= '<div style="border-top:2px solid #DDD; width:95%; min-height:50px;  margin-left:10px;padding-left:10px;padding-top:15px; padding-right: 0px;">';
													/* $recipientHtml .= '<div style=" float:left; font-size:18px; color:#000;">';
														$recipientHtml .= '<span><b>From: </b>'. $customerEmail .'</span><br/>';
													$recipientHtml .= '</div>'; */
													$recipientHtml .= '<div style="float:right; font-size:18px; color:#000;">';
														$recipientHtml .= '<span><b>Order Date: </b>'. date("d M Y").'</span><br/>';
													$recipientHtml .= '</div>';
												$recipientHtml .= '</div>';
												$recipientHtml .= '<table style="width:97%;padding-left:15px; padding-top:20px; border-collapse: collapse; font-size:16px;" align="center">';
													$recipientHtml .= '<tbody>';
														$recipientHtml .= '<tr style="border-bottom:1px solid #000; border-top:1px solid #000;">';
															$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">#</th>';
															//$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Order ID</th>';
															$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Voucher</th>';
															$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Quantity</th>';
															$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Redeem Code</th>';
															//$recipientHtml .= '<th style="text-align:center; padding-top:10px; padding-bottom:10px;">Price</th>';
														$recipientHtml .= '</tr>';
														$totalPrice = 0;
														unset($pushedData); // $foo is gone
														$pushedData = array();
														$recptIndex = 0;
														$arrOrder[]=$orderID;
														$sqlForEmail = "select order_details.*, m_voucher_details.v_name from order_details inner join m_voucher_details on order_details.voucher_id = m_voucher_details.voucher_id where order_details.order_id = $orderID and order_details.recipient_email = '".$recpEmail."'";
														$dataForEmail = mysqli_query($conn,$sqlForEmail);
														//print mysqli_num_rows($dataForEmail);
														while ($rowForEmail = mysqli_fetch_assoc($dataForEmail)) 
														{
															$arrOrderDetails[]=$rowForEmail['id'];
															$voucherSubtotal = $rowForEmail['voucher_subtotal'];
															$voucherFinalImage = $rowForEmail['voucher_final_image'];
															$recptIndex = $recptIndex + 1;
															$recipientHtml .= '<tr style="font-size:14px; border-bottom:1px solid #DDD;">';
																$recipientHtml .= '<td style="text-align:center; padding:5px 0px;">'.$recptIndex.'</td>';
																//$recipientHtml .= '<td style="text-align:center;">'.$rowForEmail['customer_order_id'].'</td>';
																$recipientHtml .= '<td style="text-align:center;">'.$rowForEmail['v_name'].'</td>';
																$recipientHtml .= '<td style="text-align:center;">'.$rowForEmail['quantity'].'</td>';
																$recipientHtml .= '<td style="text-align:center;">'.$rowForEmail['redeem_code'].'</td>';
																//$recipientHtml .= '<td style="text-align:center;">$ '.number_format($voucherSubtotal, 2).'</td>';
															$recipientHtml .= '</tr>';
															$totalPrice = $totalPrice + $voucherSubtotal;
															
															array_push($pushedData, $voucherFinalImage);
														}
														/*$recipientHtml .= '<tr style="font-size:14px; color:#000; font-weight:bold; border-bottom:1px solid #DDD;">';
															$recipientHtml .= '<td style="text-align:center;"></td>';
															//$recipientHtml .= '<td style="text-align:center;"></td>';
															$recipientHtml .= '<td style="text-align:center;"></td>';
															$recipientHtml .= '<td style="text-align:center;"></td>';
															$recipientHtml .= '<td style="text-align:center; padding:10px 0px;">TOTAL</td>';
															$recipientHtml .= '<td style="text-align:center;">$ '.number_format($totalPrice, 2).'</td>';
														$recipientHtml .= '</tr>';*/
													$recipientHtml .= '</tbody>';
												$recipientHtml .= '</table>';
											$recipientHtml .= '</div>';
											$recipientHtml .= '<div style="background-color:#ffffff; padding-bottom:20px; text-align:center;">';
												$recipientHtml .= '<table style="width:100%;">';
													$recipientHtml .= '<tbody>';
														$recipientHtml .= '<tr>';
															$recipientHtml .= '<td style="width:100%;font-size:11px;text-align:center;color:#ccc;">';
																$recipientHtml .= 'Powered by &nbsp;&nbsp;<a style="text-decoration:none!important;" href="http://www.vouchermatic.com"><span style="font-family:\'Prompt\', sans-serif; font-size : 16px; color:#3AB2BC; letter-spacing:1.5px;">Voucher</span><span style="font-family:\'Prompt\', sans-serif; font-size : 16px; color:#1A5358; letter-spacing:1.5px;">matic</span> </a>';					
															$recipientHtml .= '</td>';
														$recipientHtml .= '</tr>';
													$recipientHtml .= '</tbody>';
												$recipientHtml .= '</table>';
											$recipientHtml .= '</div>';
										$recipientHtml .= '</div>';
									$recipientHtml .= '</div>';
									//print "Count pushedData". count($pushedData)."<br>";
									unset($arrayVal);
									$arrayVal = array();
									for($j = 0; $j < count($pushedData); $j++){
										$imageName = $pushedData[$j];
										$arrayVal[] = $base_path.$imageName;
									}
									//print"------------<br>";
									//print $recipientHtml;
									//print_r($arrayVal);
									// $attachmentVal = implode(",",$arrayVal);
									$mgClient = new Mailgun('key-0199990b49e054135482be3d479c5864');
									$domain = "https://api.mailgun.net/v3/mg.vouchermatic.com";
									# Make the call to the client.
									 $mail = $mgClient->sendMessage("$domain",
											array('from' => ''.$store_name.'<admin@vouchermatic.com>',
												'to'      => ''.$recpEmail.' <'.$recpEmail.'>',
												'subject' => 'Gift Voucher',
												'html'    => $recipientHtml
											), array(
												'attachment' => $arrayVal
											));
									 if($mail) {
										$response['mailStatus'] = 1;
										$response['arrOrderDetails'] = $arrOrderDetails;
										$response['arrOrder'] = $arrOrder;
										for ($i = 0; $i < sizeof($arrOrderDetails); $i++) {
											$rowOrderDetailID =$arrOrderDetails[$i];
											$sqlUpdateOrdDet = ' UPDATE order_details SET email_sent="1" WHERE id="'.$rowOrderDetailID.'"';
											mysqli_query($conn, $sqlUpdateOrdDet);
										}
										
										// $sqlUpdateOrdDet = ' UPDATE order_details SET email_sent="1" WHERE id="'.$orderDetailID.'"';
			
										if($orderID)
										{
												 $sqlUpdateOrd = ' UPDATE m_order SET  email_sent="1" WHERE order_id="'.$orderID.'"';
												 mysqli_query($conn, $sqlUpdateOrd);
										}
									}
									else {
										$response['mailStatus'] = 0;
									}
									
								}
								$response['retPostStatus'] = 1;
									
							}
							else {
								$response['retPostStatus'] = 0;
							}
						
					}
					else {
						$response = 0;
						
					}
				}
			}
		
		// SEND MAIL TO RECIPIENT
		
	}
	else
	{
		$response = 0;
						
	}
		print json_encode($response);
}	

CloseCon($conn);
?>	