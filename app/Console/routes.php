<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/user', 'UserController@showProfile');

Route::get('/contact', 'UserController@Contact');

Route::get('/header', 'UserController@Header');

/** Home Controller **/
Route::get('/', 'HomeController@ReturnView');
Route::get('/SendMail', 'HomeController@sendEmail');
Route::get('/Delete_Session', 'HomeController@Delete_Session');
Route::get('/SendLink', 'HomeController@SendLink');
Route::get('/ChangePassword/{email}', 'HomeController@ChangePassword');
Route::get('/UpdatePassword', 'HomeController@UpdatePassword');

/** Company Controller **/
Route::get('/Company/{email}', 'CompanyController@ReturnView');
Route::get('/Register', 'CompanyController@CompanyRegister');
Route::get('/Login', 'CompanyController@Login');
Route::get('/Get_Account_Details', 'CompanyController@Get_Account_Details');
Route::get('/SaveAccountDetails', 'CompanyController@SaveAccountDetails');

/** Location Controller **/
Route::get('/{name}/Location', 'LocationController@ReturnView');
Route::get('/Add_Location', 'LocationController@Add_Location');
Route::get('/Update_location', 'LocationController@Update_location');
Route::get('/Delete_location', 'LocationController@Delete_location');
Route::get('/Select_Location', 'LocationController@Select_Location');
Route::get('/Check_location', 'LocationController@Check_location');
Route::get('/Check_editlocation', 'LocationController@Check_editlocation');
Route::get('/Get_StaffId', 'LocationController@Get_StaffId');

/** Staff Controller **/
Route::get('/{name}/Staff', 'StaffController@ReturnView');
Route::get('/Add_staff', 'StaffController@Add_staff');
Route::get('/Update_staff', 'StaffController@Update_staff');
Route::get('/Delete_staff', 'StaffController@Delete_staff');
Route::get('/Select_staff', 'StaffController@Select_staff');
Route::get('/FillLocation', 'StaffController@FillLocation');
Route::get('/Checkstaffexists', 'StaffController@Checkstaffexists');
Route::get('/EditCheckstaffexists', 'StaffController@EditCheckstaffexists');
Route::post('/Fileupload', 'StaffController@Fileupload');
Route::post('/localtime', 'StaffController@localtime');

/** Reward Controller **/
Route::get('/{name}/Reward','RewardController@ReturnView');
Route::get('/Add_reward','RewardController@Add_reward');
Route::get('/Select_reward','RewardController@Select_reward');
Route::get('/Update_reward','RewardController@Update_reward');
Route::get('/Delete_reward','RewardController@Delete_reward');
Route::get('/FillServiceType','RewardController@FillServiceType');


/** Claim Controller**/
Route::get('/{name}/ClaimForm', 'ClaimFormController@ReturnView');
Route::post('/Add_ClaimForm', 'ClaimFormController@Add_ClaimForm');
Route::get('/Get_Claim_data', 'ClaimFormController@Get_Claim_data');
Route::get('/Archive_Question', 'ClaimFormController@Archive_Question');

/** Feedback Notification Controller**/
Route::get('/{name}/FeedbackNotification', 'FeedbackNotificationController@ReturnView');
Route::post('/FdbkNotification_GetQndata', 'FeedbackNotificationController@FdbkNotification_GetQndata');
Route::post('/Add_FeedbackNotifications', 'FeedbackNotificationController@Add_FeedbackNotifications');

/** Custom Email Controller**/
Route::get('/{name}/CustomEmail', 'CustomEmailController@ReturnView');
Route::post('/GetCustomEmailList', 'CustomEmailController@GetCustomEmailList');
Route::post('/SendCustomEmail', 'CustomEmailController@SendCustomEmail');


/** Customer Claim Controller**/
Route::get('/{name}/CustomerClaimForm', 'CustomerClaimFormController@ReturnView');
Route::get('/CheckPhoneNo', 'CustomerClaimFormController@CheckPhoneNo');
Route::post('/SaveCustClaim', 'CustomerClaimFormController@SaveCustClaim');
Route::get('/{name}/Customer_Registration/{CustId}/{EmailId}', 'CustomerClaimFormController@Customer_Registration');
Route::get('/Update_Customer', 'CustomerClaimFormController@Update_Customer');
Route::get('/Get_Claimform_data', 'CustomerClaimFormController@Get_Claimform_data');

Route::get('/SendFeedbackmailwithDelayTime', 'CustomerClaimFormController@SendFeedbackmailwithDelayTime');


/* Staff Confirmation Form Reward*/
Route::get('/{name}/StaffConfirmationFormReward', 'StaffConfirmationFormRewardController@ReturnView');
Route::get('/Checkstaffpin', 'StaffConfirmationFormRewardController@Checkstaffpin');
Route::get('/ConfirmCredit', 'StaffConfirmationFormRewardController@ConfirmCredit');
Route::get('/FillServType', 'StaffConfirmationFormRewardController@FillServType');
Route::get('/FillServSubType', 'StaffConfirmationFormRewardController@FillServSubType');

/* Service Type Master*/
Route::get('/{name}/ServiceType', 'ServiceTypeController@ReturnView');
Route::get('/Add_ServiceType', 'ServiceTypeController@Add_ServiceType');
Route::get('/Select_ServiceTypes', 'ServiceTypeController@Select_ServiceTypes');
Route::get('/Delete_Servicetype', 'ServiceTypeController@Delete_Servicetype');
Route::get('/Update_ServiceType', 'ServiceTypeController@Update_ServiceType');
Route::get('/CheckServiceTypeExists', 'ServiceTypeController@CheckServiceTypeExists');
Route::get('/EditCheckServiceTypeExists', 'ServiceTypeController@EditCheckServiceTypeExists');
Route::get('/Add_NewServiceType', 'ServiceTypeController@Add_NewServiceType');
Route::get('/Select_MasterServiceTypes', 'ServiceTypeController@Select_MasterServiceTypes');
Route::get('/CheckSubTypeExists', 'ServiceTypeController@CheckSubTypeExists');
Route::post('/Update_EditTypes', 'ServiceTypeController@Update_EditTypes');

/* Staff Customer Claim form Pssword Controller */

//Route::get('/{name}/CustomerClaimformPssword', 'CustomerClaimformPsswordController@ReturnView');
//Route::get('/GET_Location', 'CustomerClaimformPsswordController@GET_Location');


Route::get('/CustomerClaimformPassword', 'CustomerClaimformPsswordController@ReturnView');
Route::get('/GET_Location', 'CustomerClaimformPsswordController@GET_Location');
Route::get('/GET_ALLLocation','CustomerClaimformPsswordController@GET_ALLLocation');
Route::get('/{name}/CustomerLogout','CustomerClaimformPsswordController@CustomerLogout');


/* Customer History*/
Route::get('/{name}/CustomerHistory', 'CustomerHistoryController@ReturnView');
Route::get('/GetCustomerloyaltyPoint', 'CustomerHistoryController@GetCustomerloyaltyPoint');
Route::get('/AdustCustomerloyaltyPoint', 'CustomerHistoryController@AdustCustomerloyaltyPoint');


/* Session out error*/
Route::get('/error', 'ErrorController@ReturnView');
Route::get('/warning', 'ErrorController@ReturnCustomerView');


/* Marketing  */
Route::get('/{name}/Marketting', 'MarkettingController@ReturnView');
Route::get('CustomerDetails', 'MarkettingController@Get_Customer_Details');
Route::get('todayscustomercount', 'MarkettingController@Todays_Customercount');
Route::get('todaysregistrationcount', 'MarkettingController@Todays_Registrationcount');
Route::get('todaysclaimcount', 'MarkettingController@Todays_Claimcount');
Route::get('SearchTemplate', 'MarkettingController@SearchTemplate');
Route::get('GetallTemplate', 'MarkettingController@Get_alltemplate');


/* Customer Registration */
Route::get('CustomerLogin', 'CustomerLogin@ReturnView');
Route::get('customer', 'CustomerLogin@CustomerHome');
Route::post('customer_login', 'CustomerLogin@customer_login');

/* Archieved Question */
Route::get('/{name}/Archieved', 'ArchievedController@ReturnView');
Route::get('ArchievedMasterdata', 'ArchievedController@ArchievedMasterdata');
Route::get('GetRestoreQuestion', 'ArchievedController@GetRestoreQuestion');
Route::post('RestoreQuestion', 'ArchievedController@RestoreQuestion');


/* 
   Route::get('customer', 'CustomerLogin@CustomerHome');
   Route::post('customer_login', 'CustomerLogin@customer_login'); 
*/


