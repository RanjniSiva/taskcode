<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		$FunctionReq = $request->func;
		
		$BlockedFunctionswithAuth = array('registerUser' , 'authentication' , 'updatepass' ,'forgetpass' , 'callcenter_forgetpass',  'authenticateLocationLogin','callcenter_authentication', 'callcenter_updatepass');
		
		if($request->header('Authorization') !== null && $request->header('Authorization') !== "" && !in_array($FunctionReq,$BlockedFunctionswithAuth)){
			
			if (Auth::guard($guard)->check()) {
				return $next($request);
			}else{
				return response('Unauthorized Key.', 401);
			}
		}
		else if($request->header('Authorization') !== null && $request->header('Authorization') !== "" && in_array($FunctionReq,$BlockedFunctionswithAuth)){
			var_dump($request->header('Authorization'));
			return response('Unauthorized Call', 401);
		}
		else if( $FunctionReq == 'registerUser' || $FunctionReq == 'authentication' || $FunctionReq == 'callcenter_authentication' || $FunctionReq == 'updatepass' ||  $FunctionReq == 'forgetpass' ||  $FunctionReq == 'callcenter_forgetpass' || $FunctionReq == 'authenticateLocationLogin' || $FunctionReq == 'callcenter_updatepass'){
			return $next($request);
		}
		else{
			return response('Unauthorized device call', 401);
		}
    }
}
