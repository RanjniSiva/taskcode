<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthManager;

use Closure;

class AuthenticateCompany
{
	
	public function __construct(AuthManager $auth) {
		$this->auth = $auth;
	}
	
	
	public function handle($request, Closure $next) {
		
		return $request->loginData;
		
		//$user = $this->auth->setRequest($request)->user();
		
		//print_r($user);

		//if ($user) {return $next($request);}

		//return response('Unauthorized.', 401);
	}	
	
}
