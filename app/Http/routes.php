<?php



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/* HOME Controller */
Route::get('/', 'HomeController@ReturnView');
Route::POST('/authentication', 'HomeController@CheckLogin');
Route::get('/register', 'HomeController@register');
Route::POST('/registerUser', 'HomeController@UserRegistration');
Route::get('/manageusers', 'HomeController@manageusers');
Route::get('/GetuserDetails', 'HomeController@GetuserDetails');
Route::get('/updateuser', 'HomeController@updateuser');
Route::get('/DeleteUser', 'HomeController@DeleteUser');



Route::get('/logout', 'HomeController@Logout');





