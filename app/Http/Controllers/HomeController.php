<?php

use App\Home;
Use MaddHatter\LaravelFullcalendar\Facades\Calendar;
 namespace App\Http\Controllers;
 use Illuminate\Database\Seeder;

use App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;


class HomeController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function ReturnView() {
		
        return view('pages.login');
    }
	
	public function register()
    {
        return view('pages.register');
    }
	
	
	
	//User Registration
    public function UserRegistration(Request $regData)
    {
		
		$regData = json_decode($_GET['regData']);
		$chkExists = DB::table('m_users')->where('email', $regData->Email)->get();
		if(count($chkExists) == 0) {
			$insertData = array(
				'firstname' => $regData->Firstname,
				'lastname' => $regData->Lastname,
				'email' => $regData->Email,
				'password' => base64_encode($regData->Password),
				'store_name' => $regData->Bussinessname,
				'user_role' => 0,
				'is_verified' => 1
			);
			
			$regdetail = DB::table('m_users')->insertGetId($insertData);
			
			$data['user_status'] = 1;
			$data['regdetail'] = $regdetail;
		}
		else {
			$data['user_status'] = 0;
		}
		
		echo json_encode($data);

    }
	
	
	
	public function CheckLogin(Request $loginData) {
		$loginData = json_decode($_GET['loginData']);
		$l_username = $loginData->username;
		$l_password = base64_encode($loginData->password);
		$chk_username = DB::table('m_users')->where('email', $l_username)->get(); // Authenticating Username
		if(count($chk_username) > 0){
			$chk_username_verified = DB::table('m_users')->where('email', $l_username)->where('is_verified', 1)->get(); // Authenticating Username
			if(count($chk_username_verified) > 0){
				$chk_pwd = DB::table('m_users')->where('email', $l_username)->where('password', $l_password)->get(); // Authenticating Password
				if(count($chk_pwd) > 0) {
					$userData = array(
						'user_id' => $chk_pwd[0]->user_id,
						'email' => $chk_pwd[0]->email,
						'store_name' => $chk_pwd[0]->store_name,
						'firstname' => $chk_pwd[0]->firstname,
						'lastname' => $chk_pwd[0]->lastname,
						'user_role' => $chk_pwd[0]->user_role
					);
					Session::push('user_details', $userData); // Set user Session
					$data['l_status'] = 3;
					$user_role = $chk_pwd[0]->user_role;
					$data['user_role'] = $user_role;
				}
				else {
					$pwdStatus = 2; // Password Invalid
					$data['l_status'] = $pwdStatus;
				}
			}
			else {
				$data['l_status'] = 1; // Not Verified
			}
		}
		else{
			$data['l_status'] = 0; // Username Invalid 
		}
		return $data;
	}
	
	public function manageusers() {
		
        return view('pages.manageusers');
    }
	
	
	
	// get user Details
	public function GetuserDetails(Request $request) {
		if(Session::has('user_details')){
			$user_details = Session::get('user_details');
			$store_id = $user_details[0]['user_id'];
			$user_role = $user_details[0]['user_role'];
			
			if($user_role == 1){
				$userlist = DB::table('m_users')
							->where('m_users.user_role', '!=' ,1)
							->get();
			}else{
				$userlist = DB::table('m_users')
							->where('m_users.user_id' ,$store_id)
							->get();
			}
			
			

			$retData['userlist'] = $userlist;
			$retData['retdata'] = 1;
		}
		else {
			$retData['retdata'] = 0;
		}
		return $retData;
	}
	
	
	
	//update Location
	public function updateuser(Request $request) {
		
		if(Session::has('user_details')){
			$user_details = Session::get('user_details');
			$store_id = $user_details[0]['user_id'];
			$user_role = $user_details[0]['user_role'];
				$userfirstname = $_GET['userfirstname'];
				$userlastname = $_GET['userlastname'];
				$userdtorename = $_GET['userdtorename'];
				$useremail = $_GET['useremail'];
				
				
				$userdata = DB::table('m_users')->where('user_id',$store_id)->get();
				if(count($userdata) == 1) {
					$updateData = array(
						'firstname' => $userfirstname,
						'lastname' => $userlastname,
						'store_name' => $userdtorename,
						'email' => $useremail
					);
					$updUserData = DB::table('m_users')->where('user_id',$store_id)->update($updateData);
					$retData['retdata'] = 1;			
				}
				
			}else{
				$retData['retdata'] = 0;
			}

		echo json_encode($retData);
    }
	
	
	
	
	
	
	
	// DELETE PRODUCT
	public function DeleteUser(Request $request) {
		if(Session::has('user_details')){
			$user_details = Session::get('user_details');
			$store_id = $user_details[0]['user_id'];
			$storeid = $_GET['storeid'];
			$userdetails = DB::table('m_users')->where('user_id',$storeid)->get();
			
			if(count($userdetails) > 0) {
				DB::table('m_users')->where('user_id',$storeid)->delete();
				$retData = 1;	
			}
			else {
				$retData = 0; 	
			}
		}else{
			$retData = 0; 	
		}
		return $retData;
	}
	
	
	
	// LOGOUT
	public function Logout(Request $request) {
		if(Session::has('user_details')){
			Session::flush();
		}
		return redirect('/');
	}
	
	
	
	
}