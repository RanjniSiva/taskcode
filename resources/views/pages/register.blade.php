	@include('layout.noheader')
	<link rel="stylesheet" href="{{URL::asset('Validation/common-validation.css')}}" />

	<script src="{{URL::asset('js/modernizr.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('Validation/commonDirective.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('js/Appjs/register.js?n=6')}}"></script>
	
	<div class="grid_frame page-content" ng-app="organization" ng-controller="registercontroller">
		<section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="account-pages">
                                <div class="account-box">
                                    <div class="account-logo-box">
                                        <h2 class="text-center">
                                            <a href="<?php echo url('/'); ?>" class="text-success">
                                                 Registration
                                            </a>
                                        </h2>
										<center>
										<h5 class="rs title-mod">Welcome to Organization</h5>
										<h5 ng-bind="errmsg"  style="color:red;"> </h5>
										<h5 ng-bind="succsmsg"  style="color:#32c861;"> </h5>
										</center>
                                        <h5 class="text-uppercase font-bold m-b-5 m-t-50">Register</h5>
                                        <p class="m-b-0">Get access to our admin panel</p>
                                    </div>
                                    <div class="account-content">
                                        <form class="form-horizontal">

                                            <div class="form-group m-b-20">
                                                <div class="col-xs-12">
                                                    <label for="username">Full Name</label>
                                                    <input class="form-control" id="sys_firstname_reg" type="text" ng-model="reg.Firstname" placeholder="Your First Name please!"/>
                                                </div>
                                            </div>

                                            <div class="form-group m-b-20">
                                                <div class="col-xs-12">
                                                    <label for="emailaddress">Last Name</label>
                                                    <input class="form-control" id="sys_Lastname_reg" type="text"  ng-model="reg.Lastname" placeholder="Your Last Name please!"/>
                                                </div>
                                            </div>

											<div class="form-group m-b-20">
                                                <div class="col-xs-12">
                                                    <label for="emailaddress">Email address</label>
                                                    <input class="form-control" id="sys_email_reg" type="email" ng-model="reg.Email" autocomplete="off" placeholder="you@mail.com" email-validate />
													<div class="input-help">
														<h5 style="color:black !important;">Email must meet the following requirements:</h5>
														<ul>
															<li ng-class="emailHasAt">must contain <strong>@</strong> symbol </li>
															<li ng-class="emailHasDot">must contain<strong> dot (.)</strong> symbol</li>
															<li ng-class="emailValid">must be in correct format</li>
														</ul>
													</div>
                                                </div>
                                            </div>
											
											<div class="form-group m-b-20">
                                                <div class="col-xs-12">
                                                    <label for="emailaddress">Business Name</label>
                                                    <input class="form-control" id="sys_bussiness_reg" type="text" ng-model="reg.Bussinessname" placeholder="Your Bussiness!"/>
                                                </div>
                                            </div>

                                            <div class="form-group m-b-20">
                                                <div class="col-xs-12">
                                                    <label for="password">Password</label>
													<input class="form-control" id="sys_pass_reg" type="password"  ng-model="reg.Password"  placeholder="Password please!" password-validate />									
													<div class="input-help">
														<h5>Password must meet the following requirements:</h5>
														<ul>
															<li ng-class="pwdHasLowerLetter">At least <strong>one lower case letter [a-z]</strong></li>
															<li ng-class="pwdHasUpperLetter">At least <strong>one upper case letter [A-Z]</strong></li>
															<li ng-class="pwdHasNumber">At least <strong>one numeric character [0-9]</strong></li>
															<li ng-class="pwdHasSpecialCharacter">At least <strong>one special character</strong></li>
															<li ng-class="pwdValidLength">should have <strong>min 8 characters and max 16 characters</strong></li>
														</ul>
													</div>
                                                </div>
                                            </div>
											<div class="form-group m-b-20">
                                                <div class="col-xs-12">

                                                    <div class="checkbox checkbox-success">
														<input id="sys_chk_agree" type="checkbox" ng-model="reg.iAgree" />
                                                        <label for="remember">
                                                            I accept <a href="#">Terms and Conditions</a>
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group text-center m-t-10">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-md btn-block btn-primary waves-effect waves-light" id="RegisterSave" ng-click="Savereg(reg)" type="button">REGISTER</button>
                                                </div>
                                            </div>

                                        </form>

                                        <div class="row m-t-10">
                                            <div class="col-sm-12 text-center">
                                                <p class="text-muted">Already have an account?  <a href="<?php echo url('/'); ?>" class="text-dark m-l-5"><b>Sign In</b></a></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- end card-box-->
                            </div>


                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
          </section>
					
					
					
					
					
					<!--
					<div class="container_grid">
						<div class="mod-register">
							<h3 class="rs title-mod">Hello pretty! Welcome to UseGiftvoucher.com</h3>
							<div class="desc-reg">Sign up for free and get exclusive access to members-only savings, rewards and special promotions from UseGiftvoucher.com. Enter in an email and a password to setup an account with Us.</div>
							<h3 ng-bind="errmsg"  style="color:red"> </h3>
							
							<div class="wrap-form-reg clearfix">
								<form action="#">
									<div class="left-form">
									<label class="wrap-txt" for="sys_firstname_reg">
											<input class="input-txt" id="sys_firstname_reg" type="text"  ng-model="reg.Firstname"   placeholder="Your First Name please!"/>
										</label>
										
										<label class="wrap-txt" for="sys_Lastname_reg">
											<input class="input-txt" id="sys_Lastname_reg" type="text"  ng-model="reg.Lastname" placeholder="Your Last Name please!"/>
										</label>
										
										<label class="wrap-txt" for="sys_email_reg">
											<input class="input-txt" id="sys_email_reg" type="email" ng-model="reg.Email" autocomplete="off" placeholder="you@mail.com" email-validate/>
										<div class="input-help">
									<h4 style="color:black !important;">Email must meet the following requirements:</h4>
									<ul>
										<li ng-class="emailHasAt">must contain <strong>@</strong> symbol </li>
										<li ng-class="emailHasDot">must contain<strong> dot (.)</strong> symbol</li>
										<li ng-class="emailValid">must be in correct format</li>
									</ul>
								</div>
										</label>
								
									
									
										
									
									</div>
									<div class="right-connect">
									<div class="wrap-form-reg clearfix">
								<form action="#">
									<div class="left-form" style="width:100%">

											<label class="wrap-txt" for="sys_bussiness_reg">
											<input class="input-txt" id="sys_bussiness_reg" type="text" ng-model="reg.Bussinessname" placeholder="Your Bussiness!"/>
										</label>
												<label class="wrap-txt" for="sys_pass_reg">
											<input class="input-txt" id="sys_pass_reg" type="password"  ng-model="reg.Password"  placeholder="Password please!" password-validate />									
	                             <div class="input-help">
									<h4>Password must meet the following requirements:</h4>
									<ul>
										<li ng-class="pwdHasLowerLetter">At least <strong>one lower case letter [a-z]</strong></li>
										<li ng-class="pwdHasUpperLetter">At least <strong>one upper case letter [A-Z]</strong></li>
										<li ng-class="pwdHasNumber">At least <strong>one numeric character [0-9]</strong></li>
										<li ng-class="pwdHasSpecialCharacter">At least <strong>one special character</strong></li>
										<li ng-class="pwdValidLength">should have <strong>min 8 characters and max 16 characters</strong></li>
									</ul>
								</div>
									</label>
										<button class="btn-flat yellow btn-submit-reg"   ng-click="Savereg(reg)" type="button">Create an account</button>
										<label class="wrap-check" for="sys_chk_agree">
											<input id="sys_chk_agree" class="input-chk" type="checkbox"    ng-model="reg.iAgree" /> I agree to the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.
											<i class="icon iUncheck"></i>
										</label>
										</form>
										</div>
										</div>
										
									
									</div>
								</form>
							</div>
							<p class="rs wrap-link-back">
							<a id="" href="./login" class="link-back"> 
									<i class="pick-r"></i>
									Back to login
								</a>
							</p>
						</div>
					</div>-->
	</div>