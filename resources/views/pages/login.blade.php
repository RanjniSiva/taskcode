	@include('layout.noheader')
	<script type="text/javascript" src="{{URL::asset('js/Appjs/menubar.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('js/Appjs/login.js?n=9')}}"></script>
	<div class="grid_frame page-content" ng-app="organization" ng-controller="HomeController">
				
		<section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">

                            <div class="account-pages">
                                <div class="account-box">
                                    <div class="account-logo-box">
                                        <h2 class="text-center">
                                            <a href="<?php echo url('/');?>" class="text-success">
                                                 login
                                            </a>
                                        </h2>
										<center>
										<h5 class="rs title-mod"></h5>
										<h5 ng-bind="loginmsg" id="msg_div" style="color:red"> </h5>
										<h5 ng-bind="loginsucsmsg"  style="color:#5eba7d"> </h5>
										</center>
                                        <h5 class="text-uppercase font-bold m-b-5 m-t-50">Sign In</h5>
                                        <p class="m-b-0">Login to your  account</p>
                                    </div>
                                    <div class="account-content">
                                        <form class="form-horizontal">
											<div ng-hide="div_login">
												<div class="form-group m-b-20">
													<div class="col-xs-12">
														<label for="emailaddress">Email address</label>
														<input class="form-control" type="email" id="sys_email" ng-model="login.username" type="email"  required="" placeholder="john@deo.com">
													</div>
												</div>

												<div class="form-group m-b-20">
													<div class="col-xs-12">
														<label for="password">Password</label>
														<input class="form-control" required=""  id="sys_pass" ng-model="login.password" type="password" placeholder="Password please!">
													</div>
												</div>

												<div class="form-group text-center m-t-10">
													<div class="col-xs-12">
														<button class="btn btn-md btn-block btn-primary waves-effect waves-light" id="StoreLogin" data-ng-click="checklogin(login)" type="submit">LOGIN</button>
													</div>
												</div>
											</div>
											
                                        </form>

										<div class="row m-t-50">
                                            <div class="col-sm-12 text-center">
                                                <p class="text-muted">Don't have an account? <a href="<?php echo url('/register'); ?>" class="text-dark m-l-5"><b>Sign Up</b></a></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->

                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
          </section>
				
        </div>