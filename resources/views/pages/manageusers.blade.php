@include('layout.loginheader')
<!-- JS REFERENCE - START -->
<link href="{{URL::asset('css/app_styles.css')}}" rel="stylesheet">
<link href="{{URL::asset('css/toggle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('css/font-awesome-min-4.7.0.css')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{URL::asset('js/Appjs/manageusers.js?n=20')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/js-base64@2.5.2/base64.min.js"></script>
<!-- JS REFERENCE - END -->
<style>
   .boxtype{
	   width: 95px;
	   padding: 8px;
	   height: 37px;
	   border: 1px solid #c1b9b9;
	   box-shadow: none !important;
	   border-radius: 3px;
   }
   .textclr{
	  color: #4489e4;
	  font-weight: bold;
   }
   .stepSpan{
   font-size:16px;
   }
   .disable_mon_border{
	    border: none;
		background: white;	   
   }
   .disable_tues_border{
	    border: none;
		background: white;	   
   }
   .disable_wed_border{
	    border: none;
		background: white;	   
   }
   .disable_thur_border{
	    border: none;
		background: white;	   
   }
   .disable_fri_border{
	    border: none;
		background: white;	   
   }
   .disable_sat_border{
	    border: none;
		background: white;	   
   }
   .disable_sun_border{
	    border: none;
		background: white;	   
   }
   .del_mon{
	   color:#fff;
   } 
   .del_tues{
	   color:#fff;
   }
   .del_wed{
	   color:#fff;
   }
   .del_thur{
	   color:#fff;
   }
   .del_fri{
	   color:#fff;
   }
   .del_sat{
	   color:#fff;
   }
   .del_sun{
	   color:#fff;
   }
   /* width */
::-webkit-scrollbar {
    width: 5px;
}
/* Track */
::-webkit-scrollbar-track {
    background: #FFF;
}
/* Handle */
::-webkit-scrollbar-thumb {
    background: #d8dbdd;
	border-radius: 5px;
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #d0d3d5; 
}
.fa{
	   cursor: pointer;
   }
</style>

<?php 
	if(Session::has('user_details')) {
		$user_details = Session::get('user_details');
		$user_id = $user_details[0]['user_id'];
		$user_role = $user_details[0]['user_role'];
		
	}
	
	
	
		
	?>
<div class="grid_frame page-content" ng-app="organization" ng-controller="manageusers">
   <div class="content-page">
      <div class="content">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="page-title-box">
                     <h4 class="page-title">User List </h4>
                     
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="card-box">
                     <div class="row">
                        <div class="col-sm-10">
                           <h4 class="m-t-0 m-b-0 "><b>User List</b></h4>
                        </div>
                        <div class="col-sm-2">
                          
                        </div>
                     </div>
                     <hr/>
                     <div class="form-inline m-b-20">
                        <div class="row">
                           <div class="col-sm-5 text-xs-center">
								<span ng-bind="StatusMsg" style="color:#32c861; font-weight: bold;"></span>
								<span ng-bind="StatusErrMsg" style="color:red; font-weight: bold;"></span>
                           </div>
                        </div>
                     </div>
                     <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0 default footable-loaded footable" data-page-size="7">
                        <thead>
                           <tr>
                              <th>Actions</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Store Name</th>
                              <th>Email</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr class="footable-even" ng-if="userlist" ng-repeat="userlists in userlist" style="display: table-row;">
                              <td class="footable-visible footable-first-column" width="90px">
							  <?php if($user_role == 0){?>
								  <a href="#" data-toggle="modal" data-target="#Edituser" style="color:grey;">
                                 <span ng-click="edituser(userlists)"><i class="fa fa-edit"></i></span>
                                 </a>
							  <?php } ?>
							  <?php if($user_role == 1){?>
                                 <a href="#" data-toggle="modal" data-target="#DelProduct" class="pull-right" style="color:grey;">
                                 <span ng-click="deleteuser(userlists.user_id)"><i class="fa fa-trash"></i></span>
                                 </a>
							  <?php } ?>
                              </td>
                              <td class="footable-visible text-primary" ng-bind="userlists.firstname" ></td>
                              <td class="footable-visible text-primary" ng-bind="userlists.lastname" ></td>
                              <td class="footable-visible text-primary" ng-bind="userlists.store_name" ></td>
                              <td class="footable-visible text-primary" ng-bind="userlists.email"></td>
                             
							 
							   
                           </tr>
                           <tr class="footable-even" ng-if="!userlist">
                              <td colspan="7" align="center">No Data Available!</td>
                           </tr>
                        </tbody>
                     </table>
					 
					 
					 
	<div id="Edituser" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content" style="width: 752px;">
            <div class="tabbable">
               <div class="modal-header p-b-0">
                  <button type="button" class="close hide-modal" data-dismiss="modal" aria-hidden="true">×</button>
               </div>
               <div class="modal-body">
                        <form class="form-horizontal" role="form">
                           <div class="form-group">
                              <label class="col-md-3 control-label">First Name</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control"  ng-model="userfirstname" maxlength="40" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Last Name</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" ng-model="userlastname" maxlength="20" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Business Name</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" ng-model="userdtorename" maxlength="10">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Email</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" ng-model="useremail"  disabled>
                              </div>
                           </div>
                        </form>
						
                  <div class="modal-footer" style="display:block" id="footer_show">
				   <div class="form-group">
					  <label class="col-md-3 control-label"></label>
					  <div class="col-md-8" style="padding: 0px;padding-right: 145px;">
						 <span ng-bind="updErrMsg" style="color:red;"></span>
						 <span ng-bind="timesucess" style="color:green;"></span>
					  </div>
				   </div>
				  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                     <button type="button" ng-click="updateuser(userfirstname,userlastname,userdtorename,useremail)" id="Updateuser" class="btn btn-custom waves-light waves-effect w-md">Save changes</button>
			   </div>
               
            </div>
            <!-- Modal-content -->
         </div>
         <!-- Modal-dialog -->
      </div>
      <!-- Modal Popup -->
   </div>
   </div>
   
   
   
					 
					 <!-- DELETE - Modal Popup -->
					<div id="DelProduct" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">Delete User</h4>
								</div>
								<div class="modal-body">
									<h5 align="center">ARE YOU SURE WANT TO DELETE THIS USER?</h5>
								</div>
								<div class="modal-footer">
									<input type="hidden" ng-model="hidstoreid" />
									<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">NO</button>
									<button type="button" data-dismiss="modal" ng-click="confirmDelete(hidstoreid)" id="DeleteUser" class="btn btn-custom waves-light waves-effect w-md">SURE</button>
								</div>
							</div><!-- Modal-content -->
						</div><!-- Modal-dialog -->
					</div><!-- Modal Popup -->
					 
                     <div class="row">
                        <div class="col-sm-12">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
  
   <!-- Modal Popup -->
</div>