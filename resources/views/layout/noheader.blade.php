<!DOCTYPE html >
<html>
    <head>
		<meta charset="utf-8" />
        <title>Organization</title>
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

      
        <link href="{{URL::asset('css/bootstrapv1.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/menu.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/responsive.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{URL::asset('js/modernizr.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/jquery-2.2.4.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/jquery-2.2.4.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/angular.min.js')}}"></script>
		
		<script type="text/javascript">
			var APP_URL = {!! json_encode(url('/')) !!};
		</script>
		
		<style>
			/* WRITE HTML INLINE STYLES FOR PAGES HERE */
			#LoginBtn{
				font-weight:bold;
				margin-top:20px;
			}
			.error{
				color:red;
			}
		</style>

    </head>	

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

            </div>
            <!-- Top Bar End -->

        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="{{URL::asset('/js/jquery.min.js')}}"></script>
        <script src="{{URL::asset('/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('/js/metisMenu.min.js')}}"></script>
        <script src="{{URL::asset('/js/waves.js')}}"></script>
        <script src="{{URL::asset('/js/jquery.slimscroll.js')}}"></script>

        <!-- Dashboard init -->
        <script src="{{URL::asset('/pages/jquery.dashboard.js')}}"></script>

        <!-- App js -->
        <script src="{{URL::asset('/js/jquery.core.js')}}"></script>
        <script src="{{URL::asset('/js/jquery.app.js')}}"></script>

    </body>
	
</html>
