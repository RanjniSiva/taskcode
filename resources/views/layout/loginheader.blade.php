<!DOCTYPE html >
<html>
    <head>
	
		<meta charset="utf-8" />
        <title>Organization</title>
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- C3 charts css -->
        <!--<link href="../plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />-->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- App css -->
        <link href="{{URL::asset('css/bootstrapv1.min.css?n=5')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/components.css?n=1')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/menu.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('css/responsive.css')}}" rel="stylesheet" type="text/css" />
		<!-- Modal - Custom box -->
		<link href="{{URL::asset('plugins/custombox/css/custombox.min.css')}}" rel="stylesheet">
		
		<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

        <script src="{{URL::asset('js/modernizr.min.js')}}"></script>
	
		<script type="text/javascript" src="{{URL::asset('js/jquery-2.2.4.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/jquery-2.2.4.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/angular.min.js')}}"></script>
		
		<link rel="stylesheet" href="{{URL::asset('plugins/sweet-alert2/sweetalert2.min.css')}}">
		<script type="text/javascript" src="{{URL::asset('plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
		
		
		<script src="https://kit.fontawesome.com/9cb30bfcf6.js" crossorigin="anonymous"></script>
		
		<script type="text/javascript">
			var APP_URL = {!! json_encode(url('/')) !!};
		</script>
		

		<style>
			#UserInfo{
				margin-top:25px;
			}
			#UserInfo label a{
				font-weight:bold;
				text-transform:uppercase;
				color:#FFF;
			}
			#CustomerLogin {
				margin-top:20px;
				margin-right:30px;
			}
			.pwrby_voucher {
				font-family: 'Prompt', sans-serif;
				font-size : 14px;
				color:#3AB2BC;
			}
			.pwrby_matic {
				font-family: 'Prompt', sans-serif;
				font-size : 14px;
				color:#1A5358;
			}
			
			/* As per Mark requirement chnaged banner color */
			
			.topbar .topbar-left{
				background-color: #00bca4 !important;
			}
			.navbar-default{
				background-color: #00bca4 !important;
			}
			.button-menu-mobile{
				background-color: #00bca4 !important;
			}
			
			
			
		</style>
    </head>
	<?php 
	if(Session::has('user_details')) {
		$user_details = Session::get('user_details');
		$user_id = $user_details[0]['user_id'];
		$store_name = $user_details[0]['store_name'];
		$FirstName = $user_details[0]['firstname'];
		$user_role = $user_details[0]['user_role'];
		
	}
	
	
	
		
	?>
	
    <body>	
        <!-- Begin page -->
        <div id="wrapper" >

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <!--<a href="index.html" class="logo"><span>Code<span>Fox</span></span><i class="mdi mdi-layers"></i></a>-->
                    <!-- Image logo -->
                    <a href="" class="logo">
                        <span>
                           Organization
                        </span>
                    </a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left nav-menu-left">
                            <li>
                                <button type="button" class="button-menu-mobile open-left waves-effect" id="slider">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                        </ul>

                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <li id="CustomerLogin">
								
                            </li>
							
							<li id="UserInfo">
									<label><a href=""><?php echo ucfirst($FirstName); ?></a></label>
                            </li>
							<li class="dropdown user-box">
                                <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
									<img src="{{URL::asset('images/icons/voice_presentation.svg')}}" alt="user-img" class="img-circle user-img">
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li><a href="<?php echo url('/'); ?>/logout">Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu Hidemenu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metisMenu nav" id="side-menu">
                            <li class="menu-title">Navigation</li>
							
							<li>
                                <a href="<?php echo url('/'); ?>/manageusers" aria-expanded="true"><i class="fa fa-gear"></i> <span> Manage Users </span> <span class="menu-arrow"></span></a>
                            </li>
							
                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
           <div class="content-page menu-slide">
                
            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



       <!-- jQuery  -->
        <script src="{{URL::asset('/js/jquery.min.js')}}"></script>
        <script src="{{URL::asset('/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('/js/metisMenu.min.js')}}"></script>
        <script src="{{URL::asset('/js/waves.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('js/Appjs/menubar.js')}}"></script>
        <script src="{{URL::asset('/js/jquery.slimscroll.js')}}"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-sanitize.min.js"></script>


        <!-- App js -->
        <script src="{{URL::asset('/js/jquery.core.js')}}"></script>
        <script src="{{URL::asset('/js/jquery.app.js')}}"></script>
      
    </body>
</html>
